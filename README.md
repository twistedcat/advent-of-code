# Advent Of Code

To run a specific day's solution :
```
yarn start {year}/{day}
```
Exemple :
```
yarn start 2020/01
```