const readline = require('readline');

const colors = {
  special: {
    reset: "\x1b[0m",
    bright: "\x1b[1m",
    dim: "\x1b[2m",
    underscore: "\x1b[4m",
    blink: "\x1b[5m",
    reverse: "\x1b[7m",
    hidden: "\x1b[8m",
  },
  font: {
    black: "\x1b[30m",
    red: "\x1b[31m",
    green: "\x1b[32m",
    yellow: "\x1b[33m",
    blue: "\x1b[34m",
    magenta: "\x1b[35m",
    cyan: "\x1b[36m",
    white: "\x1b[37m",
  },
  bg: {
    black: "\x1b[40m",
    red: "\x1b[41m",
    green: "\x1b[42m",
    yellow: "\x1b[43m",
    blue: "\x1b[44m",
    magenta: "\x1b[45m",
    cyan: "\x1b[46m",
    white: "\x1b[47m",
  }
}

const RIGHT_MARGIN = 80
const BOTTOM_MARGIN = 20

class TtyScreen {
  /**
   * @param Integer width : width of the virtual screen.
   * If null, will use the terminal width
   *
   * @param Integer height : height of the virtual screen
   *  If null, will use the terminal height
   */
  constructor(width, height) {
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })
    this.clear()
    this.cursor = { x: 0, y: 0 }
    this.tty = { width: process.stdout.columns, height: process.stdout.rows - 2 }
    this.width = width || this.tty.width
    this.height = height || this.tty.height
    this.screen = [...Array(this.height).keys()].map(_row => [...Array(this.width).keys()].map(_pixel => ({ value: ' ', color: null })))
  }

  static colors() { return colors }

  /**
   * @param Integer x : starting X position (in the virtual screen) to print
   *
   * @param Integer y : starting Y position (in the virtual screen) to print
   *
   * @param String data : string to print. Accept any length (will no print
   * if goes outside of virtual screen). Accept "\n" chars, will start again
   * to print the new line at (x, y+1)
   *
   * @param String color : color to use. (use the colors in this class)
   *
   * @param Boolean print : if false, register input in virtual screen but
   * does not refresh display
   */
  write(x, y, data, color = null, print = true) {
    const str = data.split('\n').map(line => line.split(''))
    let pos = { x, y }
    let max = { x, y }
    str.forEach(line => {
      line.forEach(char => {
        if (pos.x > max.x) { max.x = pos.x }
        if (pos.y > max.y) { max.y = pos.y }
        if (pos.x < this.width && pos.y < this.height) {
          this.screen[pos.y][pos.x] = { value: char, color: color }
        }
        pos.x += 1
      })
      pos.x = x
      pos.y += 1
    })
    if (print) {
      let start = { x: 0, y: 0 }
      if (max.x >= this.tty.width - RIGHT_MARGIN) { start.x = max.x - this.tty.width + RIGHT_MARGIN }
      if (max.y >= this.tty.height - BOTTOM_MARGIN) { start.y = max.y - this.tty.height + BOTTOM_MARGIN }
      this.printScreen(start)
    }
  }

  /**
   * @param {x: Integer, y:Integer} start : display the virtual screen,
   * starting from (x,y), in the terminal (starting from 0,0 in terminal)
   */
  printScreen(start = { x: 0, y: 0 }) {
    let pixels = []
    if (
      !(start.x >= this.width)
      && !(start.y >= this.height)
    ) {
      for (let y = start.y; y < this.height; y++) {
        pixels.push([])
        for (let x = start.x; x < this.width; x++) {
          pixels[y - start.y].push(this.screen[y][x])
        }
      }
      this.printToTty(pixels)
    } else {
      this.printToTty([[]])
    }
  }

  /**
   * clear the terminal
   */
  clear() {
    this.goToTtyStart()
    readline.clearScreenDown(process.stdout)
  }

  /**
   * Closes readline. Use this when you are finished
   * displaying things.
   */
  end() {
    readline.cursorTo(process.stdout, 0, this.tty.height)
    this.rl.close()
  }

  /**
   * Don't use this one directly.
   */
  printToTty(pixels) {
    this.goToTtyStart()
    let toDisplay = ''
    for (let y = 0; y < this.tty.height; y++) {
      for (let x = 0; x < this.tty.width; x++) {
        if (x >= pixels[0].length || y >= pixels.length) { continue }
        let pixel = pixels[y][x]
        let data = pixel.value
        if (pixel.color) {
          data = pixel.color + data + colors.special.reset
        }
        toDisplay += data
      }
      toDisplay += '\n'
    }
    this.rl.write(toDisplay + '\n')
  }

  goToTtyStart() {
    readline.cursorTo(process.stdout, 0, 0)
  }

}

module.exports = TtyScreen