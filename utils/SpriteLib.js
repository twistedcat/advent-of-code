const fs = require('fs')

const EMPTY = ' '

module.exports = {
  getEmptySprite(width, height) {
    return {
      pic: (EMPTY.repeat(width) + '\n').repeat(height),
      width,
      height
    }
  },
  getSprite(name) {
    const sprite = fs.readFileSync(__dirname + '/sprites/' + name).toString().split('\n')
    const result = {
      pic: sprite.join('\n'),
      width: Math.max(...sprite.map(line => line.length)),
      height: sprite.length
    }
    return result
  },
  padTop(sprite, top) {
    sprite.pic = (EMPTY.repeat(sprite.width) + '\n').repeat(top) + sprite.pic
    sprite.height += top
    return sprite
  },
  setWidth(sprite, width) {
    if (width <= sprite.width) { return sprite }
    const left = Math.ceil((width - sprite.width) / 2)
    return {
      pic: sprite.pic.split('\n').map(line => (EMPTY.repeat(left) + line).padEnd(width, EMPTY)).join('\n'),
      height: sprite.height,
      width
    }
  },
  setHeight(sprite, height) {
    if (height <= sprite.height) { return sprite }
    const top = Math.ceil((height - sprite.height) / 2)
    const bottom = height - sprite.height - top
    return {
      pic: (EMPTY.repeat(sprite.width) + '\n').repeat(top) + sprite.pic + ('\n' + EMPTY.repeat(sprite.width)).repeat(bottom),
      width: sprite.width,
      height
    }
  },
  setSize(sprite, width, height) {
    return this.setWidth(this.setHeight(sprite, height), width)
  }
}