const TtyScreen = require('../../utils/TtyScreen')
const SpriteLib = require('../../utils/SpriteLib')

const fs = require('fs')
const input = fs.readFileSync(__dirname + '/input').toString().split("\n").map(line => line.split(''))

const width = input[0].length
const height = input.length

const SPRITE_WIDTH = 8
const SPRITE_HEIGHT = 6
const sprites = {
  skier: SpriteLib.setSize(SpriteLib.getSprite('skier'), SPRITE_WIDTH, SPRITE_HEIGHT),
  firTree: SpriteLib.setSize(SpriteLib.getSprite('firTree'), SPRITE_WIDTH, SPRITE_HEIGHT),
  jump: SpriteLib.setSize(SpriteLib.getSprite('skierJump'), SPRITE_WIDTH, SPRITE_HEIGHT),
  empty: SpriteLib.getEmptySprite(SPRITE_WIDTH, SPRITE_HEIGHT)
}

console.log(sprites.skier.pic)

function isTree(char) {
  return char === '#'
}

function getCharSprite(char) {
  switch (char) {
    case '#':
      return { sprite: sprites.firTree, color: TtyScreen.colors().font.green };
    default:
      return { sprite: sprites.empty, color: null };
  }
}

function getCharAt(x, y, screen) {
  return input[y][x % width]
}

function getNbTreeHit(movRight, movDown, print = false) {
  let screen = null
  if (print) {
    const nbMov = Math.ceil(height / movDown)
    const fieldWidth = movRight * nbMov
    const nbInput = Math.ceil(fieldWidth / width)
    screen = new TtyScreen(fieldWidth * SPRITE_WIDTH, input.length * SPRITE_HEIGHT)
    const pos = { x: 0, y: 0 }
    input.forEach(line => {
      line.join('').repeat(nbInput).split('').forEach(char => {
        let toDisplay = getCharSprite(char)
        screen.write(pos.x * SPRITE_WIDTH, pos.y * SPRITE_HEIGHT, toDisplay.sprite.pic, toDisplay.color, false)
        pos.x += 1
      })
      pos.x = 0
      pos.y += 1
    })
    screen.printScreen()
  }
  let [x, y, nbTrees] = [0, 0, 0]
  let patchSprite = { pic: sprites.skier.pic, color: TtyScreen.colors().font.cyan }
  while (y < height - movDown) {
    screen.write(x * SPRITE_WIDTH, y * SPRITE_HEIGHT, patchSprite.pic, patchSprite.color, false)
    x += movRight
    y += movDown
    if (isTree(getCharAt(x, y, screen))) {
      patchSprite = { pic: sprites.firTree.pic, color: TtyScreen.colors().font.green }
      screen.write(x * SPRITE_WIDTH, y * SPRITE_HEIGHT, sprites.jump.pic, TtyScreen.colors().font.magenta)
      nbTrees++
    } else {
      patchSprite = { pic: sprites.empty.pic, color: null }
      screen.write(x * SPRITE_WIDTH, y * SPRITE_HEIGHT, sprites.skier.pic, TtyScreen.colors().font.cyan)
    }
  }
  if (screen) { screen.end() }
  return nbTrees
}

const part1Result = getNbTreeHit(3, 1, true)

// const part2Results = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]].map(coeffs => getNbTreeHit(coeffs[0], coeffs[1], true))

// console.log(`\n\n## PART 1:\nnumber of trees on the path: ${part1Result}`)
// console.log(`\n## PART 2:\nnumber of trees on the path of each slope: ${part2Results}`)
// console.log(`Multiplication result: ${part2Results.reduce((a, b) => a * b)}\n`)
