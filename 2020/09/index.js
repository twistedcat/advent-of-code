const fs = require('fs')
const numbers = fs.readFileSync(__dirname + '/input').toString().split('\n').map(i => parseInt(i))


function isSumFrom(nb, list) {
  return list.some((v1, i) => list.some((v2, j) => (i !== j) && (v1 + v2 === nb)))
}

function sumFromAdj(nb, list) {
  result = false
  list.some((v, i) => {
    let elems = [v]
    for (let i2 = i + 1; i2 < list.length; i2++) {
      elems.push(list[i2])
      sum = elems.reduce((a, b) => a + b)
      if (sum > nb) { return false }
      if (sum === nb) {
        result = elems
        return true
      }
    }
  })
  return result
}

/**
 * @param nIndex: index of the number in the "numbers" array
 * @param pSize: size of the preamble
 */
function isValid(nIndex, pSize) {
  return isSumFrom(numbers[nIndex], numbers.slice(nIndex - pSize, nIndex))
}

let preambleSize = 25
let invalid = null
for (let n = preambleSize; n < numbers.length; n++) {
  if (!isValid(n, preambleSize)) {
    invalid = numbers[n]
    break
  }
}

console.log(`\n## PART 1:\nInvalid number: ${invalid}`)

const elems = sumFromAdj(invalid, numbers)
const [min, max] = [Math.min(...elems), Math.max(...elems)]
console.log(`\n## PART 2:\nfirst number: ${min}, last number: ${max}, total: ${min + max}`)