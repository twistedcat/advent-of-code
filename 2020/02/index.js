const fs = require('fs')
const input = fs.readFileSync(__dirname + '/input').toString().split("\n").map(line => {
  const [policy, password] = line.split(': ')
  const [iMin, iMax] = policy.split(' ')[0].split('-').map(n => parseInt(n))
  return { iMin, iMax, letter: policy.split(' ')[1], password }
})

let nbValidP1 = 0
input.forEach(i => {
  const nbLEtterOcc = (i.password.match(new RegExp(i.letter, "g")) || []).length
  if ((i.iMin <= nbLEtterOcc) && (nbLEtterOcc <= i.iMax)) { nbValidP1 += 1 }
})

let nbValidP2 = 0
input.forEach(i => {
  if (i.password.charAt(i.iMin - 1) === i.letter ^ i.password.charAt(i.iMax - 1) === i.letter) { nbValidP2 += 1 }
})

console.log(`\n***PART 1:\nNumber of valid passwords in input: ${nbValidP1}\n`)
console.log(`\n***PART 2:\nNumber of valid passwords in input: ${nbValidP2}\n`)