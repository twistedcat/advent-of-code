const fs = require('fs')
const instructions = fs.readFileSync(__dirname + '/input').toString().split('\n').map(i => ({
  order: i[0],
  value: parseInt(i.substring(1))
}))

const directions = {
  'N': { x: 0, y: -1 },
  'E': { x: 1, y: 0 },
  'S': { x: 0, y: 1 },
  'W': { x: -1, y: 0 },
}

const rotations = {
  0: pos => pos,
  90: ({ x, y }) => ({ x: -1 * y, y: x }),
  180: ({ x, y }) => ({ x: -1 * x, y: -1 * y }),
  270: ({ x, y }) => ({ x: y, y: -1 * x }), // same as -90
}

function goToPoint(start, point, nbTimes) {
  return { x: start.x + point.x * nbTimes, y: start.y + point.y * nbTimes }
}

function exec(i, pointToMove) {
  if (Object.keys(directions).includes(i.order)) {
    ship[pointToMove] = goToPoint(ship[pointToMove], directions[i.order], i.value)
  }
  else if (i.order === 'F') { ship.position = goToPoint(ship.position, ship.waypoint, i.value) }
  else if (['R', 'L'].includes(i.order)) {
    ship.waypoint = rotations[(i.value * (i.order === 'L' ? -1 : 1) + 360) % 360](ship.waypoint)
  }
}

let ship = { position: { x: 0, y: 0 }, waypoint: directions.E }
instructions.forEach(i => exec(i, 'position'))
console.log(`\n** PART 1:\nManhattan distance from start: ${Math.abs(ship.position.x) + Math.abs(ship.position.y)}`)

ship = { position: { x: 0, y: 0 }, waypoint: { x: 10, y: -1 } }
instructions.forEach(i => exec(i, 'waypoint'))
console.log(`\n** PART 2:\nManhattan distance from start: ${Math.abs(ship.position.x) + Math.abs(ship.position.y)}\n`)