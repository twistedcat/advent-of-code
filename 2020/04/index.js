const fs = require('fs')
const passports = fs.readFileSync(__dirname + '/input').toString().split('\n\n').map(line => getPassport(line))

function getPassport(data) {
  return data.split(/[\s\n]+/).reduce((acc, currentData) => {
    const [key, value] = currentData.split(':')
    acc[key] = value
    return acc
  }, {})
}

function isPassportValid(p, rules) {
  return Object.keys(rules).every(fieldKey => {
    return Object.keys(p).includes(fieldKey) && rules[fieldKey](p[fieldKey])
  })
}

function intBetween(i, min, max) {
  value = parseInt(i)
  if (!value) { return false }
  return (value >= min) && (value <= max)
}

const PART_1_RULES = {
  byr: v => true,
  iyr: v => true,
  eyr: v => true,
  hgt: v => true,
  hcl: v => true,
  ecl: v => true,
  pid: v => true,
}
console.log(`\n## PART 1.\nNumber of valid passports: ${passports.filter(p => isPassportValid(p, PART_1_RULES)).length}`)

const PART_2_RULES = {
  byr: v => intBetween(v, 1920, 2002),
  iyr: v => intBetween(v, 2010, 2020),
  eyr: v => intBetween(v, 2020, 2030),
  hgt: v => {
    if (v.endsWith('cm')) { return intBetween(v.slice(0, -2), 150, 193) }
    if (v.endsWith('in')) { return intBetween(v.slice(0, -2), 59, 76) }
    return false
  },
  hcl: v => /^#([0-9a-f]{6})$/i.test(v),
  ecl: v => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(v),
  pid: v => /^([0-9]{9})$/.test(v),
}
console.log(`\n## PART 2.\nNumber of valid passports: ${passports.filter(p => isPassportValid(p, PART_2_RULES)).length}\n`)