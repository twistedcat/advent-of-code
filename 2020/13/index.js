const fs = require('fs')
const [[departure], lines] = fs.readFileSync(__dirname + '/input').toString()
  .split('\n').map(v => v.split(',').map(v => parseInt(v)))

let constraints = []
for (const [index, line] of lines.entries()) {
  if (line) { constraints.push({ val: line, offset: index }) }
}
constraints.sort((c1, c2) => c2.val - c1.val)

function getWaitTime(timestamp, line) {
  let passedTime = timestamp % line
  return passedTime ? line - timestamp % line : 0
}

const startPart1 = process.hrtime()
let nextBus = { line: null, waitTime: null }
constraints.forEach(c => {
  const waitTime = getWaitTime(departure, c.val)
  if (!nextBus.line || waitTime < nextBus.waitTime) {
    nextBus = { line: c.val, waitTime }
  }
})
const endPart1 = process.hrtime(startPart1)
console.log(`\n** PART 1:\nNext bus is line ${nextBus.line}, departs in ${nextBus.waitTime} minutes.`)
console.log(`Multiplication result: ${nextBus.line * nextBus.waitTime}`)
console.info(`Computation time: ${endPart1[0]}s and ${endPart1[1] / 1000}µs\n`)


function isValid(timestamp, constraint) {
  return (timestamp + constraint.offset) % constraint.val === 0
}

function findFirstMatch(c1, c2) {
  let timestamp = 0
  while (true) {
    if (isValid(timestamp, c1) && isValid(timestamp, c2)) {
      return timestamp
    }
    timestamp++
  }
}

const startPart2 = process.hrtime()
let timestamp = findFirstMatch(constraints[0], constraints[1])
let step = constraints[0].val * constraints[1].val

let constraintIndex = 2 // 0 and 1 already solved ^
let solution = 0

while (!solution) {
  if (isValid(timestamp, constraints[constraintIndex])) {
    step *= constraints[constraintIndex].val
    constraintIndex += 1
  }
  if (constraintIndex === constraints.length) { solution = timestamp }
  else { timestamp += step }
}
const endPart2 = process.hrtime(startPart2)

console.log(`\n** PART 2: first timestamp to satisfy all constraints: ${solution}`)
console.info(`Computation time: ${endPart2[0]}s and ${endPart2[1] / 1000}µs\n`)
