const fs = require('fs')
const start = fs.readFileSync(__dirname + '/input').toString().split(',').map(n => parseInt(n))

function getLastSpoken(nbTurns) {
  const spoken = {}
  for (let turn = 0; turn < start.length; turn++) {
    spoken[start[turn]] = turn
  }

  let last = 0
  for (let turn = start.length; turn < nbTurns - 1; turn++) {
    const newLast = spoken[last] === void 0 ? 0 : turn - spoken[last]
    spoken[last] = turn
    last = newLast
  }
  return last
}

let NB_TURNS = 2020
console.log(`\n** PART 1:\n${NB_TURNS}th number spoken: ${getLastSpoken(NB_TURNS)}`)

NB_TURNS = 30000000
console.log(`\n** PART 2:\n${NB_TURNS}th number spoken: ${getLastSpoken(NB_TURNS)}\n`)