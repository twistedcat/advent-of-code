const fs = require('fs')
const allAnswers = fs.readFileSync(__dirname + '/input').toString().split('\n\n').map(grpAnswers => getGroupYesQuestions(grpAnswers))

function getGroupYesQuestions(grpAnswers) {
  return grpAnswers.split('\n').reduce((acc, curr) => {
    curr.split('').forEach(a => acc.answers[a] = acc.answers[a] ? acc.answers[a] + 1 : 1)
    return { answers: acc.answers, nbPers: acc.nbPers + 1 }
  }, { answers: {}, nbPers: 0 })
}

const part1 = allAnswers.reduce((acc, curr) => acc + Object.values(curr.answers).length, 0)
const part2 = allAnswers.reduce((acc, curr) => acc + Object.values(curr.answers).filter(a => a >= curr.nbPers).length, 0)

console.log(`\n## PART 1:\nTotal number of questions answerd yes at least once in all groups: ${part1}`)
console.log(`\n## PART 2:\nTotal number of questions whole group answerd yes in all groups: ${part2}\n`)