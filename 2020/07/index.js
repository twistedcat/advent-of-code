const fs = require('fs')
const allRules = fs.readFileSync(__dirname + '/input').toString().split('\n').map(ruleStr => getRule(ruleStr)).reduce((acc, rule) => {
  acc[rule.color] = rule.contained
  return acc
}, {})

function getRule(r) {
  let [color, contained] = r.split(' contain ')
  contained = contained.split(', ').map(c => {
    const [nb, color] = c.replace(/\s+/, '\x01').split('\x01')
    return { nb: parseInt(nb), color: color.replace(/\sbags?\.?/, '') }
  }).filter(c => c.nb)
  return { color: color.replace(/\sbags?\.?/, ''), contained }
}

function hasColor(color, needleColor) {
  if (!allRules[color].length) { return false }
  if (allRules[color].filter(r => r.color === needleColor).length) { return true }
  return allRules[color].some(r => hasColor(r.color, needleColor))
}

function countBags(color, root = true) {
  if (!allRules[color].length) { return 1 }
  return allRules[color].reduce((acc, r) => acc + r.nb * (countBags(r.color, false)), 0) + (root ? 0 : 1)
}

const part1 = Object.keys(allRules).reduce((acc, color) => acc + hasColor(color, 'shiny gold'), 0)
const part2 = countBags('shiny gold')

console.log(`\n## PART 1:\nColors that contains at least one shiny gold bag: ${part1}`)
console.log(`\n## PART 1:\nNumber of bags in a shiny gold bag: ${part2}\n`)
