const fs = require('fs')
let [rules, myTicket, nearbyTickets] = fs.readFileSync(__dirname + '/input').toString().split('\n\n')

function parseTicket(t) {
  return t.split(',').map(v => parseInt(v))
}

rules = rules.split('\n').map(rule => {
  let [field, ranges] = rule.split(': ')
  ranges = ranges.split(' or ').map(range => {
    const [min, max] = range.split('-').map(v => parseInt(v))
    return { min, max }
  })
  return { field, ranges }
})
const flatRules = rules.map(r => r.ranges).flat()

myTicket = parseTicket(myTicket.replace('your ticket:\n', ''))
nearbyTickets = nearbyTickets.replace('nearby tickets:\n', '').split('\n').map(t => parseTicket(t))

function getInvalidValues(ticket) {
  let invalids = []
  ticket.forEach(v => {
    if (flatRules.every(r => v < r.min || v > r.max)) {
      invalids.push(v)
    }
  });
  return invalids
}

const part1 = nearbyTickets.map(t => getInvalidValues(t)).flat().reduce((a, b) => a + b)
console.log(`\n** PART 1:\nScanning error rate: ${part1}`)

const validTickets = [myTicket, ...nearbyTickets.filter(t => !getInvalidValues(t).length)]

let constraints = [...Array(myTicket.length).keys()].map(_ => rules)
for (let fieldIndex = 0; fieldIndex < myTicket.length; fieldIndex++) {
  validTickets.forEach(ticket => {
    constraints[fieldIndex] = constraints[fieldIndex].filter(rule => {
      return rule.ranges.some(range => range.min <= ticket[fieldIndex] && range.max >= ticket[fieldIndex])
    })
  })
}

constraints = constraints.map((c, i) => ({ index: i, values: c })).sort((c1, c2) => c1.values.length - c2.values.length)
let usedFields = []
let remainingConstraints = []
constraints.forEach((constraint) => {
  remainingConstraints.push({
    index: constraint.index,
    value: constraint.values.filter(c => !usedFields.includes(c.field))[0]
  })
  usedFields.push(remainingConstraints[remainingConstraints.length - 1].value.field)
})

const part2 = remainingConstraints
  .reduce((acc, curr) => curr.value.field.startsWith('departure') ? acc * myTicket[curr.index] : acc, 1)
console.log(`\n** PART 2:\nMultiplication of 'departure' fields values: ${part2}\n`)