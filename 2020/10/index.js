const fs = require('fs')
const ratings = fs.readFileSync(__dirname + '/input').toString().split('\n').map(i => parseInt(i)).sort((a, b) => a - b)

const ratingDiffs = ratings.reduce((acc, r, rIndex) => {
  if (rIndex === 0) { acc[1] = r }
  const accIndex = rIndex + 1 === ratings.length ? 3 : ratings[rIndex + 1] - r
  acc[accIndex] += 1
  return acc
}, { 1: 0, 2: 0, 3: 0 })

function getPermutations(arr) {
  arr.unshift(0); arr.push(arr[arr.length - 1] + 3) // add outlet and device ratings
  return arr.reduce((acc, r, rIndex) => {
    if (rIndex === 0) { return acc }
    r - ratings[rIndex - 1] === 1
      ? acc.streak += 1
      : acc = { count: acc.count *= getPermNb(acc.streak + 1), streak: 0 }
    return acc
  }, { count: 1, streak: 0 }).count
}

function getPermNb(length) {
  return (length > 2 ? length - 2 : 0) // permutations with one number
    + (length > 3 ? [...Array(length - 2).keys()].reduce((a, b) => a + b) : 0) // permutations with two number
    + 1 //+1 because no permutation is a valid permutation
}

console.log(`\n## PART 1:\nDifferences: 1:${ratingDiffs[1]}, 3:${ratingDiffs[3]}\nMultiplication: ${ratingDiffs[1] * ratingDiffs[3]}`)
console.log(`\n## PART 2:\nNumber of valid permutations: ${getPermutations(ratings)}\n`)