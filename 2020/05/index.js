const fs = require('fs')
const seatsIds = fs.readFileSync(__dirname + '/input').toString().split('\n').map(s => getSeatId(s))

function binaryToDec(num, one, zero) {
  return parseInt(num.replaceAll(zero, '0').replaceAll(one, '1'), 2)
}

function getSeatId(seat) {
  return 8 * binaryToDec(seat.slice(0, 7), 'B', 'F') + binaryToDec(seat.slice(-3), 'R', 'L')
}

const maxSeatId = Math.max(...seatsIds)
const mySeatId = [...Array(maxSeatId).keys()].filter(sId => seatsIds.includes(sId - 1) && !seatsIds.includes(sId) && seatsIds.includes(sId + 1))

console.log(`\n## PART 1:\nHighest seat ID: ${maxSeatId}`)
console.log(`\n## PART 2:\nMy seat ID: ${mySeatId}\n`)