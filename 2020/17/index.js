const part1 = require('./part1')
const part2 = require('./part2')

console.log(`\n** PART 1:\nNumber of active cubes: ${part1.result}`)
console.log(`\n** PART 2:\nNumber of active cubes: ${part2.result}\n`)