const fs = require('fs')
let start = {
  values: [fs.readFileSync(__dirname + '/input').toString().split('\n').map(line => line.split('').map(v => [v]))],
  offset: { w: 0, x: 0, y: 0, z: 0 }
}

const ACTIVE = '#'
const INACTIVE = '.'

function newState(pos, grid) {
  let activeNeighbors = 0
  const zLength = grid.values.length
  const yLength = grid.values[0].length
  const xLength = grid.values[0][0].length
  const wLength = grid.values[0][0][0].length
  for (let dz = -1; dz <= 1; dz++) {
    for (let dy = -1; dy <= 1; dy++) {
      for (let dx = -1; dx <= 1; dx++) {
        for (let dw = -1; dw <= 1; dw++) {
          if (
            (pos.x + dx < 0) || (pos.y + dy < 0) || (pos.z + dz < 0) || (pos.w + dw < 0)
            || (pos.x + dx >= xLength) || (pos.y + dy >= yLength) || (pos.z + dz >= zLength) || (pos.w + dw >= wLength)
            || (dx === 0 && dy === 0 && dz === 0 && dw === 0)
          ) { continue }
          if (grid.values[pos.z + dz][pos.y + dy][pos.x + dx][pos.w + dw] === ACTIVE) { activeNeighbors++ }
          if (activeNeighbors > 3) { return INACTIVE }
        }
      }
    }
  }
  if (activeNeighbors === 3) { return ACTIVE }
  const current = grid.values[pos.z][pos.y][pos.x][pos.w]
  if (current === ACTIVE && activeNeighbors === 2) { return ACTIVE }
  return INACTIVE
}

function getEmptyPlane(w, x, y) {
  return [...Array(y).keys()].map(_l => getEmptyLine(w, x))
}

function getEmptyLine(w, x) {
  return [...Array(x).keys()].map(_hl => getEmptyHyperline(w))
}

function getEmptyHyperline(w) {
  return [...Array(w).keys()].map(_ => INACTIVE)
}

function padLine(line, pad = 1) {
  return [
    getEmptyHyperline(line[0].length + pad * 2),
    ...line.map(hl => [
      ...INACTIVE.repeat(pad).split(''),
      ...hl,
      ...INACTIVE.repeat(pad).split('')]),
    getEmptyHyperline(line[0].length + pad * 2),
  ]
}

function padPlane(plane, pad = 1) {
  return [
    getEmptyLine(plane[0][0].length + pad * 2, plane[0].length + pad * 2),
    ...plane.map(line => padLine(line)),
    getEmptyLine(plane[0][0].length + pad * 2, plane[0].length + pad * 2)
  ]
}

function padGrid(grid, pad = 1) {
  const emptyPlane = getEmptyPlane(
    grid.values[0][0][0].length + pad * 2,
    grid.values[0][0].length + pad * 2,
    grid.values[0].length + pad * 2
  )
  return {
    offset: { w: grid.offset.w - 1, x: grid.offset.x - 1, y: grid.offset.y - 1, z: grid.offset.z - 1 },
    values: [emptyPlane, ...grid.values.map(plane => padPlane(plane, pad)), emptyPlane]
  }
}

function evolve(grid, pad = 1) {
  let paddedGrid = padGrid(grid, pad)
  let newGrid = JSON.parse(JSON.stringify(paddedGrid))
  for (let z = 0; z < paddedGrid.values.length; z++) {
    for (let y = 0; y < paddedGrid.values[0].length; y++) {
      for (let x = 0; x < paddedGrid.values[0][0].length; x++) {
        for (let w = 0; w < paddedGrid.values[0][0][0].length; w++) {
          newGrid.values[z][y][x][w] = newState({ x, y, z, w }, paddedGrid)
        }
      }
    }
  }
  return newGrid
}

function countActive(grid) {
  let active = 0
  for (let z = 0; z < grid.values.length; z++) {
    for (let y = 0; y < grid.values[0].length; y++) {
      for (let x = 0; x < grid.values[0][0].length; x++) {
        for (let w = 0; w < grid.values[0][0].length; w++) {
          if (grid.values[z][y][x][w] === ACTIVE) { active++ }
        }
      }
    }
  }
  return active
}

const NB_CYCLES = 6
let grid = JSON.parse(JSON.stringify(start))
for (let cycle = 0; cycle < NB_CYCLES; cycle++) {
  grid = evolve(grid)
}

module.exports = { result: countActive(grid) }


