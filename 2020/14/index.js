const fs = require('fs')

const INST = { MASK: 'mask', MEM: 'mem' }
const memBlockLength = 36

const instructions = fs.readFileSync(__dirname + '/input').toString().split('\n').map(line => {
  const [name, value] = line.split(' = ')
  let result = { name, value }
  if (name.includes(INST.MEM)) {
    result.binary = getBinary(value)
    result.address = name.replace('mem[', '').replace(']', '')
    result.name = INST.MEM
  }
  return result
})

function applyMask(v, mask, ignoreChars = ['X']) {
  let result = v.split('')
  for (let i = mask.length - 1; i >= 0; i--) {
    if (!ignoreChars.includes(mask[i])) {
      result[i] = mask[i]
    }
  }
  return result.join('')
}

function getBinary(n) {
  return Number(n).toString(2).padStart(memBlockLength, '0')
}

function getBothPossibilities(binary, i) {
  console.log
  return [binary, binary].map((v, vIndex) => {
    let newV = v.split('')
    newV[i] = vIndex
    return newV.join('')
  })
}

function getXIndices(mask) {
  let indices = [];
  for (var i = 0; i < mask.length; i++) {
    if (mask[i] === "X") indices.push(i);
  }
  return indices
}

function getMemoryAddresses(binary, floats) {
  const newFloats = JSON.parse(JSON.stringify(floats))
  const currentFloat = newFloats.pop()
  return newFloats.length
    ? getMemoryAddresses(binary, newFloats).map(address => getBothPossibilities(address, currentFloat)).flat()
    : getBothPossibilities(binary, currentFloat)
}

function exec(i, part1 = true) {
  switch (i.name) {
    case INST.MASK:
      state.mask = i.value
      break;
    case INST.MEM:
      part1
        ? state.memory[i.address] = applyMask(i.binary, state.mask)
        : getMemoryAddresses(
          applyMask(getBinary(i.address), state.mask, ['X', '0']),
          getXIndices(state.mask)
        ).forEach(address => state.memory[address] = i.binary)
      break;
  }
}

function sumState(s) {
  return Object.values(s.memory).reduce((acc, v) => acc + parseInt(v, 2), 0)
}

var state = { mask: '', memory: {} }
instructions.forEach(i => exec(i))
console.log(`\n** PART 1:Sum of numbers in memory: ${sumState(state)}`)

state = { mask: '', memory: {} }
instructions.forEach(i => exec(i, false))
console.log(`\n** PART 2:Sum of numbers in memory: ${sumState(state)}\n`)