const fs = require('fs')
const initialSetup = fs.readFileSync(__dirname + '/input').toString().split('\n').map(row => row.split(''))

const FLOOR = '.'
const SEAT_FREE = 'L'
const SEAT_OCCUPIED = '#'

/**
 * @param {x,y} pos : position of the central seat
 * @param {dX,dY} dir direction to look
 * @param [row][seat] seats
 */
function adjOccupied(pos, dir, seats) {
  if (
    (pos.x + dir.dX < 0 || pos.x + dir.dX >= seats[0].length)
    || (pos.y + dir.dY < 0 || pos.y + dir.dY >= seats.length)
  ) { return false }
  if (seats[pos.y + dir.dY][pos.x + dir.dX] === SEAT_OCCUPIED) { return true }
  return false
}

/**
 * @param {x,y} pos : start point of the ray
 * @param {dX,dY} dir direction of the ray
 * @param [row][seat] seats
 */
function castRayOccupied(pos, dir, seats) {
  while (true) {
    if (
      (pos.x + dir.dX < 0 || pos.x + dir.dX >= seats[0].length)
      || (pos.y + dir.dY < 0 || pos.y + dir.dY >= seats.length)
    ) {
      return false
    } else if (seats[pos.y + dir.dY][pos.x + dir.dX] !== FLOOR) {
      return seats[pos.y + dir.dY][pos.x + dir.dX] === SEAT_OCCUPIED
    }
    pos = { x: pos.x + dir.dX, y: pos.y + dir.dY }
  }
}

/**
* Count occupied seats as seen from position (x,y)
* use the function "occupiedFn" to count
*/
function countOccupiedFrom(x, y, seats, occupiedFn) {
  let occ = 0
  for (let dY = -1; dY <= 1; dY++) {
    for (let dX = -1; dX <= 1; dX++) {
      if (dX === 0 && dY === 0) { continue }
      if (occupiedFn({ x, y }, { dX, dY }, seats)) { occ++ }
    }
  }
  return occ
}

function countAllOccupied(seats) {
  return seats.reduce((acc, row) => {
    return acc + row.filter(seat => seat === SEAT_OCCUPIED).length
  }, 0)
}

function execRound(seats, occupiedFn, leaveThreshold) {
  let result = JSON.parse(JSON.stringify(seats))
  let diff = 0
  for (let y = 0; y < seats.length; y++) {
    for (let x = 0; x < seats[0].length; x++) {
      let occ = countOccupiedFrom(x, y, seats, occupiedFn)
      if (seats[y][x] === SEAT_OCCUPIED && occ >= leaveThreshold) {
        result[y][x] = SEAT_FREE
        diff++
      } else if (seats[y][x] === SEAT_FREE && occ === 0) {
        result[y][x] = SEAT_OCCUPIED
        diff++
      }
    }
  }
  return { seats: result, diff }
}

function processOccupancy(seats, occupiedFn, leaveThreshold) {
  let diff = true
  let currentSeats = seats
  while (diff) {
    let roundResult = execRound(currentSeats, occupiedFn, leaveThreshold)
    currentSeats = roundResult.seats
    diff = !!roundResult.diff
  }
  return currentSeats
}

part1Result = processOccupancy(initialSetup, adjOccupied, 4)
console.log(`\nPART 1:\nOccupied seats after stabilisation: ${countAllOccupied(part1Result)}`)

part2Result = processOccupancy(initialSetup, castRayOccupied, 5)
console.log(`\nPART 2:\nOccupied seats after stabilisation: ${countAllOccupied(part2Result)}`)