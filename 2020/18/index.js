const fs = require('fs')
let exprs = fs.readFileSync(__dirname + '/input').toString().split('\n')

function op(t1, o, t2) {
  switch (o) {
    case '+':
      return t1 + t2
    case '*':
      return t1 * t2
  }
}

function computeP2(expr) {
  let newExpr = expr
  const uniqueMatches = (expr.match(/([0-9]+\s+\+\s+)+[0-9]+/g) || [])
    .filter((m, i, self) => self.indexOf(m) === i)
    .sort((m1, m2) => m2.length - m1.length)
  for (m of uniqueMatches) {
    newExpr = newExpr.replaceAll(m, eval(m))
  }
  return eval(newExpr).toString() //keep the same expr length
}

function computeP1(expr) {
  let arr = expr.split(/\s+/)
  let result = parseInt(arr[0])
  for (let charIndex = 2; charIndex < arr.length; charIndex += 2) {
    const term = parseInt(arr[charIndex])
    result = op(result, arr[charIndex - 1], term)
  }
  return result.toString()
}

function getInsideParentheses(str) {
  return (str.match(/\(([0-9]+\s(\+|\*)\s)+[0-9]+\)/g) || []).filter((m, i, self) => self.indexOf(m) === i)
}

function evaluate(expr, computeFn) {
  let newExpr = expr
  let matches = getInsideParentheses(newExpr)
  while (matches.length) {
    for (m of matches) {
      newExpr = newExpr.replaceAll(m, computeFn(m.replace('(', '').replace(')', '')))
    }
    matches = getInsideParentheses(newExpr)
  }
  return computeFn(newExpr)
}

function evalAll(computeFn) {
  let results = []
  for (const e of exprs) {
    results.push(parseInt(evaluate(e, computeFn)))
  }
  return results
}

console.log(`\n** PART 1:\nAddition of all results: ${evalAll(computeP1).reduce((a, b) => a + b)}`)
console.log(`\n** PART 2:\nAddition of all results: ${evalAll(computeP2).reduce((a, b) => a + b)}\n`)
