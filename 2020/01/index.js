const fs = require('fs')
const input = fs.readFileSync(__dirname + '/input').toString().split("\n").map(i => parseInt(i))

function findNSum(array, target, nbPart, previousSum = 0, level = 1, usedIndexes = []) {
  for (const i in array) {
    if (usedIndexes.includes(i)) { continue }
    if (level === nbPart) {
      if (previousSum + array[i] === target) { return [array[i]] }
    } else if (previousSum + array[i] < target) {
      const lowerLevelsSolution = findNSum(array, target, nbPart, previousSum + array[i], level + 1, [...usedIndexes, i])
      if (lowerLevelsSolution) { return [...lowerLevelsSolution, array[i]] }
    }
  }
  return false
}

function printSolution(solution) {
  solution && solution.length
    ? console.log(`Solution: ${solution.join(', ')}. Multiplication result: ${solution.reduce((a, b) => a * b)}`)
    : console.log('No solution found.')
}

console.log('\n*** PART 1')
printSolution(findNSum(input, 2020, 2))

console.log('\n*** PART 2')
printSolution(findNSum(input, 2020, 3))

console.log('\n')