const fs = require('fs')
const instructions = fs.readFileSync(__dirname + '/input').toString().split('\n').map(i => {
  const [instruction, value] = i.split(' ')
  return { instruction, value: parseInt(value) }
})

const ERROR = -1
const OK = 0

let history = []
let accumulator = 0
let current = 0

function logError() {
  console.log(`\nWARNING: Infinite loop detected, circle back to line ${current}.`)
  console.log(`Line ${current} already seen on instruction ${history.indexOf(current)}.`)
  console.log(`${history.length} instructions executed.`)
}

function switchInstruction(i) {
  switch (i) {
    case 'nop':
      return 'jmp'
    case 'jmp':
      return 'nop'
    default:
      return i;
  }
}

function switchNext(switched) {
  for (let l = lastSwitch(switched) + 1; l < instructions.length; l++) {
    if (['nop', 'jmp'].includes(instructions[l].instruction)) {
      // console.log(`Switching instruction on line ${l} from ${instructions[l].instruction} to ${switchInstruction(instructions[l].instruction)}`)
      instructions[l].instruction = switchInstruction(instructions[l].instruction)
      return l
    }
  }
  console.log('WARNING: No instruction to switch')
  return ERROR
}

function cancelLastSwitch(switched) {
  if (!switched.length) {
    // console.log('Nothing to cancel')
    return
  }
  let lastSwitched = lastSwitch(switched)
  // console.log(`Canceling last switch on line ${lastSwitched}: reset ${instructions[lastSwitched].instruction} to ${switchInstruction(instructions[lastSwitched].instruction)}`)
  instructions[lastSwitched].instruction = switchInstruction(instructions[lastSwitched].instruction)
}

function lastSwitch(switched) {
  if (!switched.length) { return -1 }
  return switched[switched.length - 1]
}

interpretor = {
  acc(val) { accumulator += val; current += 1 },
  nop(val) { current += 1 },
  jmp(val) { current += val },
  reset() { history = []; accumulator = 0; current = 0 },
  exec(log = false) {
    while (current < instructions.length) {
      if (history.includes(current)) {
        if (log) { logError() }
        return ERROR
      }
      history.push(current)
      interpretor[instructions[current].instruction](instructions[current].value)
    }
    return OK
  }
}


interpretor.exec()
console.log(`\n## PART 1:\nAccumulator value before infinite loop: ${accumulator}`)

interpretor.reset()
let switchedIntructions = []
while (interpretor.exec() === ERROR) {
  cancelLastSwitch(switchedIntructions)
  switchedIntructions.push(switchNext(switchedIntructions))
  interpretor.reset()
}
console.log(`\n## PART 2:\nAccumulator value with fixed loop: ${accumulator}\n`)